import { generate } from "@ant-design/colors";

const reds = generate("#ff0000");

export const colors = {
  primary: reds[4],
  secondary: reds[0],
  tertiary: reds[2],
};
