import { useEffect } from "react";
import { Row, Col, Tabs } from "antd";
import SportsCard from "./Card";
import { DashboardStore } from "../store/DashboardStore";
import "../styling/betting-dashboard.css";
import { SPORT_TYPES, SportGame } from "../types";
import logo from "../logo.svg";
import useMediaQuery from "../hooks/useMediaQuery";

export const getSportGames = async () => {
  try {
    const response = await fetch("http://localhost:3000/sportGames");
    const data = await response.json();
    DashboardStore.update((s) => {
      s.sportGames = data;
    });
  } catch (e) {
    console.error(e);
  }
};
const onlyUnique = (value: any, index: any, array: string | any[]) => {
  return array.indexOf(value) === index;
};

const BettingDashboard = () => {
  const sportGames = DashboardStore.useState((s) => s.sportGames);

  useEffect(() => {
    getSportGames();
  }, []);

  const filterSportGamesByType = (type: SPORT_TYPES) => {
    if (!sportGames) return [];
    return sportGames.filter((game: SportGame) => game.type === type);
  };
  const sportTypes = Object.values(
    sportGames.map((sportGame: SportGame) => sportGame.type)
  ).filter(onlyUnique);

  return (
    <div style={{ padding: "20px 20px 20px 20px" }}>
      <img src={logo} alt="Logo" width={200} height={100} />
      <Tabs type="card" animated={true}>
        <Tabs.TabPane tab="ALL" key="ALL">
          <Row gutter={[24, 24]}>
            {sportGames?.map((sportGame: SportGame, index: number) => (
              <Col key={index} xs={24} sm={24} md={24} lg={24} xl={24}>
                <SportsCard sportGame={sportGame} />
              </Col>
            ))}
          </Row>
        </Tabs.TabPane>
        {sportTypes?.map((sportType) => (
          <Tabs.TabPane tab={sportType} key={sportType}>
            <Row gutter={[24, 24]}>
              {filterSportGamesByType(sportType).map((sportGame, index) => (
                <Col key={index} xs={24} sm={24} md={12} lg={24} xl={24}>
                  <SportsCard sportGame={sportGame} />
                </Col>
              ))}
            </Row>
          </Tabs.TabPane>
        ))}
      </Tabs>
    </div>
  );
};

export default BettingDashboard;
