import React, { useState } from "react";
import { Badge, Button, Card, Col, Row, message } from "antd";
import BettingModal from "./Modal";
import { getSportGames } from "./BettingDashboard";
import { colors } from "../constants/colors";
import { BettingForm, SportGame, Team } from "../types";
import useMediaQuery from "../hooks/useMediaQuery";

type SportsCardProps = {
  sportGame: SportGame;
};

const SportsCard: React.FC<SportsCardProps> = ({ sportGame }) => {
  const { teams } = sportGame;
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const [messageApi, contextHolder] = message.useMessage();
  const isMobile = useMediaQuery(576);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const updateTeam = async (teamId: number) => {
    try {
      await fetch(`http://localhost:3000/sportGames/${sportGame.id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          ...sportGame,
          teams: teams.map((team: Team) => {
            if (team.id === teamId) {
              return { ...team, totalBets: team.totalBets + 1 };
            } else {
              return { ...team };
            }
          }),
        }),
      });
      messageApi.open({
        type: "success",
        content: "Successfully placed bet",
      });
      getSportGames();
    } catch (e) {
      console.error(e);
    }
  };

  const handleSave = (data: BettingForm) => {
    console.log({ data });
    updateTeam(data.team);
    setIsModalVisible(false);
  };

  const firstTeam = teams[0];
  const secondTeam = teams[1];

  return (
    <>
      <Badge.Ribbon text={sportGame.type} color={colors.tertiary}>
        {contextHolder}
        <Card
          size="small"
          style={{
            height: isMobile ? 200 : 100,
            backgroundColor: colors.secondary,
          }}
        >
          <Row>
            <Col span={isMobile ? 24 : 0} sm={2} md={1} lg={1} xl={1}>
              <img width={40} alt="sport" src={sportGame.image} />
            </Col>
            <Col span={isMobile ? 24 : 0} sm={18} md={20} lg={20} xl={21}>
              <div style={{ display: "flex" }}>
                <b style={{ marginRight: "4px" }}>{firstTeam.name}</b>
                <Badge count={firstTeam.odds} showZero />
                <b style={{ margin: "0 4px" }}>vs</b>
                <b style={{ marginRight: "4px" }}>{secondTeam.name}</b>
                <Badge count={secondTeam.odds} />
              </div>
              <div>
                Total bets for {firstTeam.name}:{" "}
                <Badge count={firstTeam.totalBets} showZero />
              </div>
              <div>
                Total bets for {secondTeam.name}:{" "}
                <Badge count={secondTeam.totalBets} showZero />
              </div>
            </Col>
            <Col sm={1} md={1} lg={1} xl={1}>
              <Button
                type="primary"
                onClick={() => showModal()}
                style={{
                  marginTop: 20,
                  backgroundColor: colors.primary,
                }}
              >
                <b>BET NOW</b>
              </Button>
            </Col>
            <BettingModal
              teams={teams}
              visible={isModalVisible}
              onCancel={handleCancel}
              onSave={handleSave}
            />
          </Row>
        </Card>
      </Badge.Ribbon>
    </>
  );
};

export default SportsCard;
