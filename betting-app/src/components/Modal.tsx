import { Modal, Form, Select, InputNumber } from "antd";
import { colors } from "../constants/colors";
import { BettingForm, Team } from "../types";

type BettingModalProps = {
  visible: boolean;
  onCancel: () => void;
  onSave: (values: BettingForm) => void;
  teams: Team[];
};

const BettingModal: React.FC<BettingModalProps> = ({
  visible,
  onCancel,
  onSave,
  teams,
}) => {
  const [form] = Form.useForm<BettingForm>();

  const handleSave = () => {
    form
      .validateFields()
      .then((values) => {
        onSave({ ...values });
        form.resetFields();
      })
      .catch((errorInfo) => {
        console.log("Validation failed:", errorInfo);
      });
  };

  return (
    <Modal
      title="Place your bet"
      open={visible}
      onOk={handleSave}
      okText="Place bet"
      onCancel={onCancel}
      okButtonProps={{ style: { backgroundColor: colors.primary } }}
    >
      <Form form={form} layout="vertical">
        <Form.Item
          name="team"
          label="Team"
          rules={[{ required: true, message: "Please select a team" }]}
          style={{ width: "50%" }}
        >
          <Select
            options={teams.map((team: Team) => {
              return { value: team.id, label: team.name };
            })}
          />
        </Form.Item>
        <Form.Item
          name="amount"
          label="Amount ($)"
          rules={[
            { required: true, message: "Please enter an amount" },
            {
              type: "number",
              min: 0,
              message: "Amount must be greater than 0",
            },
          ]}
        >
          <InputNumber addonBefore="+" addonAfter="$" />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default BettingModal;
