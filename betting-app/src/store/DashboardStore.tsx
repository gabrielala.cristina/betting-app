import { Store } from "pullstate";
import { SportGame, DashboardStoreType } from "../types";

export const DashboardStore = new Store<DashboardStoreType>({
  sportGames: [] as SportGame[],
});
