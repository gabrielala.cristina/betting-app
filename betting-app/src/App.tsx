import { Suspense } from "react";
import BettingDashboard from "./components/BettingDashboard";
import { colors } from "./constants/colors";

const App = () => {
  return (
    <Suspense>
      <div style={{ height: "100%" }}>
        <BettingDashboard />
      </div>
    </Suspense>
  );
};

export default App;
