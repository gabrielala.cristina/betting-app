export enum SPORT_TYPES {
  FOOTBALL = "FOOTBALL",
  BASKETBALL = "BASKETBALL",
  TENNIS = "TENNIS",
}

export interface Team {
  id: number;
  name: string;
  odds: number;
  totalBets: number;
}

export interface SportGame {
  id: number;
  type: SPORT_TYPES;
  image: string | undefined;
  teams: Team[];
}

export interface DashboardStoreType {
  sportGames: SportGame[];
}

export interface BettingForm {
  team: number;
  amount: number;
}
