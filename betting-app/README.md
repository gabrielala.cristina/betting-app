# Getting Started with Betting App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

Before running the project:
### `cd betting-app`
### `npm install`

For running the project locally:

### `npm start`
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

### `json-server --watch db.json`
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

